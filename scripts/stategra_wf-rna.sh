#! /bin/bash
#cd "$(dirname "$0")"

echo 'Object: Workflow for the RNAseq analysis'
echo 'Inputs: stategra fastq files'
echo 'Outputs: Trimmed files, alignment, feature counts, DESEQ2 analysis'

# Handling errors
#set -x # debug mode on
set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation
#set -o verbose

IFS=$'\n\t'

start_time=$(date +%s)

# Initial QC
jid1=$(sbatch --parsable /uca_m2bi_hpc/scripts/stategra_rnaseq_qc-init.slurm)

echo "$jid1 : Initial Quality Control"




# Trimming
jid2=$(sbatch --parsable --dependency=afterok:$jid1 /uca_m2bi_hpc/scripts/stategra_rnaseq_trim.slurm)

echo "$jid2 : Trimming with Trimmomatic tool"




# Post QC
jid3=$(sbatch --parsable --dependency=afterok:$jid2 /uca_m2bi_hpc/scripts/stategra_rnaseq_qc-post.slurm)

echo "$jid3 : Post control_quality"




# Alignment
jid4=$(sbatch --parsable --dependency=afterok:$jid3 /uca_m2bi_hpc/scripts/stategra_rnaseq_alignement.slurm)

echo "$jid4 : Alignment with Bowtie2"



# Multiqc
jid5=$(sbatch --parsable --dependency=afterok:$jid4 /uca_m2bi_hpc/scripts/stategra_rnaseq_multiqc.slurm)

echo "$jid5 : Multiqc"


# Samtools
jid6=$(sbatch --parsable --dependency=afterok:$jid5 /uca_m2bi_hpc/scripts/stategra_rnaseq_read-count.slurm)

echo "$jid6 : Samtools"


# HTseq-count
jid7=$(sbatch --parsable --dependency=afterok:$jid6 /uca_m2bi_hpc/scripts/stategra_rnaseq_read-count.slurm)

echo "$jid7 : Features count with htseq-count"


# DESEQ2
jid8=$(sbatch --parsable --dependency=afterok:$jid7 /uca_m2bi_hpc/scripts/stategra_rnaseq_DESEQ2.slurm)

echo "$jid8 : DESEQ2 Analysis"


# Ending step considering job efficiency
# seff

