Ce projet est réalisé dans le cadre de l'UE M2-S3 de bioinformatique "Calculs parallèles et 
programmation GPU" pour la partie HPC. L'ensemble de ce workflow est fait pour être utilisé
à partir d'un "git clone https://gitlab.com/maxime_arque/uca_m2bi_hpc.git"

Ce workflow déposé sur git permet de traiter des données de RNA-seq issues de la publication de
Green, D., Tarazona, S., Ferreirós-Vidal, I., Ramírez, R. N., Schmidt, A., Reijmers, T. H., 
Von Saint Paul, V., Marabita, F., Rodríguez-Ubreva, J., García-Gómez, A., Carroll, T., Cooper, L., 
Liang, Z., Dharmalingam, G., Van Der Kloet, F., Harms, A. C., Balzano‐Nogueira, L., Lagani, V., 
Tsamardinos, I.,. . . Conesa, A. (2019b). STATEGra, a comprehensive multi-omics dataset of B-cell
 differentiation in mouse. Scientific Data, 6(1).  https://doi.org/10.1038/s41597-019-0202-7 .

Cette analyse est réalisée sous clusters HPC avec des scripts qui sont interprétés par Slurm.

Trois environnements conda ("DESEQ2", "samtools", "htseq") sont utilisés est disponibles au format yml si besoin est de reproduire l'analyse. Pour les utiliser :

module load conda/4.12.0
conda env create -n DESEQ2 -f conda_DESEQ2.yml
conda env create -n samtools -f conda_samtools.yml
conda env create -n htseq -f conda_htseq.yml
(yml dans le dossier conda_env_YML)

conda activate DESEQ2 ---> pour le script "stategra_rnaseq_DESEQ2.slurm"
conda activate samtools ---> pour le script "stategra_rnaseq_filter.slurm"
conda activate htseq ---> pour le script "stategra_rnaseq_read-count.slurm"


Les scripts ne fonctionneront que si l'utilisateur respecte l'arborescence suivante :

 uca_m2bi_hpc
     │
     ├── conda_env_YML
     │   ├── conda_DESEQ2.yml
     │   └── conda_samtools.yml
     ├── data
     │   └── rna-seq -> /home/users/shared/data/stategra/rna-seq/
     ├── log
     ├── README.md
     ├── results
     └── scripts
         ├── Script_DESEQ2.R
         ├── stategra_rnaseq_alignement.slurm
         ├── stategra_rnaseq_DESEQ2.slurm
         ├── stategra_rnaseq_filter.slurm
         ├── stategra_rnaseq_multiqc.slurm
         ├── stategra_rnaseq_qc-init.slurm
         ├── stategra_rnaseq_qc-post.slurm
         ├── stategra_rnaseq_read-count.slurm
         ├── stategra_rnaseq_trim.slurm
         └── stategra_wf-rna.sh


Tous les scripts sont fait pour être lancés dans le home (c'est à dire à l'extérieur du fichier
uca_m2bi_hpc) et dans l'ordre suivant :



1) stategra_rnaseq_qc-init.slurm
--> Récupère les fastq.gz de rna-seq et réalise un contrôle qualité
INPUT : /home/users/shared/data/stategra/rna-seq/*
OUTPUT : /uca_m2bi_hpc/results/rna-seq/qc/*/fastqc-init

2) stategra_rnaseq_trim.slurm
--> A partir des fastq.gz, réalise un trimming de ces séquences pour nettoyage
INPUT : /home/users/shared/data/stategra/rna-seq/*
OUTPUT : /uca_m2bi_hpc/results/rna-seq/trim

3) stategra_rnaseq_qc-post.slurm
--> Utilise les fastq.gz trimmés pour réaliser un nouveau contrôle qualité
INPUT : /uca_m2bi_hpc/results/rna-seq/trim/*
OUPUT : /uca_m2bi_hpc/results/rna-seq/qc/*/fastqc-post

4) stategra_rnaseq_multiqc.slurm
--> Réalise un multiqc de contrôle qualité pré-trim vs post-trim
INPUT : /uca_m2bi_hpc/results/rna-seq/qc/*
OUTPUT : /uca_m2bi_hpc/results/rna-seq/qc/multiqc

5) stategra_rnaseq_alignement.slurm
--> Aligne tous les fastq.gz sur le génome de référence avec bowtie2 / hisat2 (bowtie est en 
commentaire mais fonctionnel)
INPUT : /uca_m2bi_hpc/results/rna-seq/trim/*paired*
OUTPUT : /uca_m2bi_hpc/results/rna-seq/alignment

6) stategra_rnaseq_filter.slurm
--> Filtre et trie les lectures de l'alignement
INPUT : /uca_m2bi_hpc/results/rna-seq/alignment
OUTPUT : /uca_m2bi_hpc/results/rna-seq/samtools

7) stategra_rnaseq_read-count.slurm
--> Comptage des features de l'alignement avec htseq-count
INPUT : /uca_m2bi_hpc/results/rna-seq/samtools
OUTPUT : /uca_m2bi_hpc/results/rna-seq/htseq

8) stategra_rnaseq_DESEQ2.slurm
--> Réalise un analyse de différentiel d'expression de gènes avec DESEQ2
INPUT : /uca_m2bi_hpc/results/rna-seq/htseq
OUTPUT : /uca_m2bi_hpc/results/rna-seq/DESEQ2
/!\ Le script prend longtemps si les packages ne sont pas installé parceque il les installes
tous. Parcontre si ils sont déjà installés le script prend moins de 1 minute.

Les scripts fonctionnent aussi seulement si les raw data sont organisées de la même manière que sur le 
serveur (/home/users/shared/data/stategra/rna-seq/) et idem pour génome de référence, index,  etc.

/!\ Si jamais le script stategra_rnaseq_DESEQ2.slurm ne fonctionne pas à cause des packages, il faut 
installer les packages en étant dans le conda DESEQ2, en ouvrant R puis en tapant les commandes 
suivantes : 

if (!requireNamespace("BiocManager", quietly = TRUE)) {
  install.packages("BiocManager")
}
library(BiocManager)
BiocManager::install("DESeq2")
BiocManager::install("apeglm")
BiocManager::install("dplyr")
BiocManager::install("gridExtra")



/!\ Comme nous vous en avions parlé en cours, pour les données de RNA-seq on a un problème de 
"badly formatted array jobid -2" dont on ne connait pas l'origine. Ca nous empêche d'utiliser le script
"stategra_wf-rna.sh" qui lance tous les scripts les uns après les autres. On a essayé d'utiliser nos 
scripts sur d'autres types de données et cela fonctionne sur tout sauf la rna-seq. Le seul moyen 
d'éviter d'avoir cette erreur en rna seq est de mettre #SBATCH --cpus-per-task = 1. On ne l'a pas fait 
pour pouvoir optimiser la durée de nos jobs et la parallélisation et même si l'erreur apparait, les
tâches se réalisent bel et bien. (cf squeue et logs)

Si jamais certaines données ne sortent pas, elles sont quand même disponibles sur le serveur (student12)

